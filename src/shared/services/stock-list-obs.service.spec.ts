/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { StockListObsService } from './stock-list-obs.service';

describe('Service: StockListObs', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StockListObsService]
    });
  });

  it('should ...', inject([StockListObsService], (service: StockListObsService) => {
    expect(service).toBeTruthy();
  }));
});
