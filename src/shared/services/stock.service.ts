import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { forkJoin, Observable } from 'rxjs';
import { SentimentModel } from '../models/sentiment.model';
import { CompanyNameResultModel, ForkJoinResultModel, QuoteDataModel } from '../models/stock.model';
import { StockListObsService } from './stock-list-obs.service';

@Injectable({ providedIn: 'root' })
export class StockService {

    static token: string = 'bu4f8kn48v6uehqi3cqg';

    constructor(private httpClient: HttpClient, private toastr: ToastrService, private stockListObsService: StockListObsService) { }

    /**
     * Get stock informations and company name by symbol.
     * If exists, add it to stockList.
     * If not, find the closest company and suggest it to user.
     * @param stockSymbol symbol asked by user (string).
     */
    addStockData(stockSymbol: string): void {
        forkJoin(
            {
                name: this.httpClient.get<CompanyNameResultModel>("https://finnhub.io/api/v1/search?q=" + stockSymbol + "&token=" + StockService.token),
                quoteData: this.httpClient.get<QuoteDataModel>("https://finnhub.io/api/v1/quote?symbol=" + stockSymbol + "&token=" + StockService.token)
            }
        ).subscribe({
            next: (result) => {
                // Check if symbol found
                const companyName = (result as ForkJoinResultModel).name?.result?.find((nameResult) => nameResult.displaySymbol === stockSymbol)?.description;
                if (companyName) {
                    // Symbol found, add it to stockList
                    const stockToAdd = {
                        symbol: stockSymbol,
                        name: companyName,
                        quoteData: result.quoteData
                    };
                    this.stockListObsService.addToStockList(stockToAdd);
                } else {
                    // Symbol not found, notify tu user.
                    let message = "Hum, we didn't found " + stockSymbol + " symbol.";
                    if (result.name?.count > 0) {
                        message += " Did you mean " + result.name.result[0].displaySymbol + " (" + result.name?.result[0].description + ") ?";
                    }
                    this.toastr.error(message);
                    this.stockListObsService.nullPropagation();
                }
            },
            error: (e) => {
                this.toastr.error("Sorry, something went wrong : " + e.error?.error);
                this.stockListObsService.nullPropagation();
            }
        });
    }

    /**
     * Get montly sentiment data for a symbol.
     * @param symbol Symbol (string)
     * @param from Begining of period (yyyy-mm-dd)
     * @param to End of period (yyyy-mm-dd)
     * @returns List of sentiment data (one per month), and list of company name related to the passed symbol.
     */
    getSentimentData(symbol: string, from: string, to: string): Observable<{
        sentimentData: SentimentModel,
        name: CompanyNameResultModel
    }> {
        const urlSentiment: string = "https://finnhub.io/api/v1/stock/insider-sentiment?symbol=" + symbol + "&from=" + from + "&to=" + to + "&token=" + StockService.token;
        const urlName: string = "https://finnhub.io/api/v1/search?q=" + symbol + "&token=" + StockService.token;
        return forkJoin({
            sentimentData: this.httpClient.get<SentimentModel>(urlSentiment),
            name: this.httpClient.get<CompanyNameResultModel>(urlName)
        });
    }

}
