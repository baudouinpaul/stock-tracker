import { Injectable } from '@angular/core';
import { TrendEnum } from 'src/app/home/stock-card/stock-card.model';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor() { }

  /**
   * Return the trend state of a numeric value (up, down, stable). 
   * @param value Numeric value
   * @returns Corresponding trend state (TrendEnum). Null if Nan.
   */
  getTrendOf(value: number): TrendEnum {
    if (value > 0) {
      return TrendEnum.UP;
    }
    if (value < 0) {
      return TrendEnum.DOWN;
    }
    if (value === 0) {
      return TrendEnum.STABLE;
    }
    return null;
  }

  /**
   * Return the name of the month which number is passed as param.
   * @param monthNumber Number of the month (1 to 12).
   * @returns Month's name. Null if out of range.
   */
  getMonthNameByNumber(monthNumber: number): string {
    const monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    if (monthNumber > 0 && monthNumber <= 12) {
      return monthNames[monthNumber];
    }
    return null;
  }

}
