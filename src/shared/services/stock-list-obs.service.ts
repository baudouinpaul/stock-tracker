import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { StockModel } from '../models/stock.model';

@Injectable({
  providedIn: 'root'
})
export class StockListObsService {

  private stockListSubject = new Subject<StockModel[]>();
  private stocks: StockModel[] = [];

  /**
   * Allows to subscribe to the stockListSubject subject.
   * @returns Observable of StockModel[]
   */
  getStockList(): Observable<StockModel[]> {
    return this.stockListSubject.asObservable();
  }

  /**
   * Add a StockModel to the list, and emit a new stockListSubject observable.
   * Add the symbol to the localStorage list.
   * @param stockToAdd StockModel to add
   */
  addToStockList(stockToAdd: StockModel): void {
    this.stocks.push(stockToAdd);
    this.stockListSubject.next(this.stocks);
    this.updateLocalStorage();
  }

  /**
   * Remove a StockModel from the list (from his symbol), and emit a new stockListSubject observable.
   * Remove the symbol of the localStorage list.
   * @param stockSymbol Symbol to remove
   */
  removeFromStockListBySymbol(stockSymbol: string): void {
    this.stocks = this.stocks.filter((stock: StockModel) => stock.symbol !== stockSymbol);
    this.stockListSubject.next(this.stocks);
    this.updateLocalStorage();
  }

  /**
   * Allows to emit a null Obsevable. Use in case of error for exemple. 
   */
  nullPropagation(): void {
    this.stockListSubject.next(null);
  }

  /**
   * Reset the stock list.
   */
  resetStockList(): void {
    this.stocks = [];
  }

  /**
   * Update the symbol list that is stocked in localStorage. Based on stocks list.
   */
  updateLocalStorage(): void {
    const currentSymbols = this.stocks.map((stock: StockModel) => stock.symbol);
    localStorage.setItem("stock-tracker", JSON.stringify(currentSymbols));
  }

}
