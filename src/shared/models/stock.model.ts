export interface StockModel {
    symbol: string;
    name: string;
    quoteData: QuoteDataModel;
}

// See https://finnhub.io/docs/api/quote
export interface QuoteDataModel {
    c: number; // Current price
    d: number; // Change
    h: number; // High price of the day
    l: number; // Low price of the day
    o: number; // Open price of the day
    pc: number; // Previous close price
}

export enum QuoteKeysEnum {
    CURRENT_PRICE = 'c',
    CHANGE = 'd',
    HIGH_PRICE = 'h',
    LOW_PROCE = 'l',
    OPEN_PRICE = 'o',
    PREVIOUS_CLOSE_PRICE = 'pc'
}

export interface ForkJoinResultModel {
    name: CompanyNameResultModel;
    quoteData: QuoteDataModel;
}

export interface CompanyNameResultModel {
    count: number;
    result: CompanyNameModel[];
}

export interface CompanyNameModel {
    description: string;
    displaySymbol: string;
    symbol: string;
    type: string;
}