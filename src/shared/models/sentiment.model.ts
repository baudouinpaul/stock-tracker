// See https://finnhub.io/docs/api/insider-sentiment
export interface SentimentModel {
    data: SentimentDataModel[]; // Array of sentiment data.
    symbol: string; // Symbol of the company.
}

export interface SentimentDataModel {
    symbol: string; // Symbol.
    year: number; // Year.
    month: number; // Month.
    change: number; // Net buying/selling from all insiders' transactions.
    mspr: number; // Monthly share purchase ratio.
}