import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { StockService } from 'src/shared/services/stock.service';

@Component({
  selector: 'app-search-card',
  templateUrl: './search-card.component.html',
  styleUrls: ['./search-card.component.scss']
})
export class SearchCardComponent implements OnInit {

  @Output() newSearch: EventEmitter<void> = new EventEmitter();

  searchFormControl: FormControl;

  constructor(private stockService: StockService, private toastrService: ToastrService) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(): void {
    this.searchFormControl = new FormControl(null, [Validators.required, Validators.pattern('^[a-zA-Z-]{1,5}$')]);
  }

  onSubmitForm(): void {
    if (this.searchFormControl.valid) {
      const search: string = this.searchFormControl.value.toUpperCase();
      // Check if already shown
      const currentSymbols: string[] = JSON.parse(localStorage.getItem("stock-tracker")) || [];
      if (currentSymbols.includes(search)) {
        this.toastrService.error("You already added this symbol.");
      } else {
        this.newSearch.emit();
        this.stockService.addStockData(search);
        this.searchFormControl.setValue(null);
      }
    }
  }

}
