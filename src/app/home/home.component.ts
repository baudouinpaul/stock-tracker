import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { StockModel } from 'src/shared/models/stock.model';
import { StockListObsService } from 'src/shared/services/stock-list-obs.service';
import { StockService } from 'src/shared/services/stock.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  stockToShow: StockModel[];
  subscription: Subscription;
  loadingBuffer: number = 0;

  constructor(private stockListObsService: StockListObsService, private stockService: StockService) { }

  ngOnInit() {
    this.subscribeStocksToShow();
    this.verifyLocalStorage();
  }

  subscribeStocksToShow(): void {
    this.subscription = this.stockListObsService.getStockList().subscribe({
      next: (stockList) => {
        if (stockList !== null) {
          this.stockToShow = stockList.sort((a, b) => a.name?.localeCompare(b.name));
        }
        this.loadingBuffer--;
      }
    });
  }

  verifyLocalStorage(): void {
    const currentSymbols: string[] = JSON.parse(localStorage.getItem("stock-tracker")) || [];
    for (let symbol of currentSymbols) {
      this.loadingBuffer++;
      this.stockService.addStockData(symbol);
    }
  }

  onNewSearch(): void {
    this.loadingBuffer++;
  }

  onRemoveCard(): void {
    this.loadingBuffer++;
  }

  trackByFn(index: number, item: StockModel) {
    return item.symbol; // item symbol is used as single key
  }

  ngOnDestroy(): void {
    this.stockListObsService.resetStockList();
    this.subscription.unsubscribe();
  }

}
