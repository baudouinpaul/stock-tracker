import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { QuoteDataModel, QuoteKeysEnum, StockModel } from 'src/shared/models/stock.model';
import { StockListObsService } from 'src/shared/services/stock-list-obs.service';
import { UtilsService } from 'src/shared/services/utils.service';
import { CardTypeEnum, StockCardBoxInterface, TrendEnum } from './stock-card.model';

@Component({
  selector: 'app-stock-card',
  templateUrl: './stock-card.component.html',
  styleUrls: ['./stock-card.component.scss']
})
export class StockCardComponent implements OnChanges {

  @Input() data: StockModel;
  @Output() removeCard: EventEmitter<void> = new EventEmitter();

  public CardTypeEnum = CardTypeEnum;
  public TrendEnum = TrendEnum;
  dataBoxToShow: StockCardBoxInterface[] = [];
  currentTrend: TrendEnum;
  // Groups of data to show
  overviewQuotes: { key: QuoteKeysEnum, name: string, type: CardTypeEnum }[] = [
    {
      key: QuoteKeysEnum.CHANGE,
      name: "Change today",
      type: CardTypeEnum.PCT
    },
    {
      key: QuoteKeysEnum.OPEN_PRICE,
      name: "Opening price",
      type: CardTypeEnum.MONEY
    },
    {
      key: QuoteKeysEnum.CURRENT_PRICE,
      name: "Current price",
      type: CardTypeEnum.MONEY
    },
    {
      key: QuoteKeysEnum.HIGH_PRICE,
      name: "High price",
      type: CardTypeEnum.MONEY
    },
  ];

  constructor(private stockListObsService: StockListObsService, private router: Router, private utilsService: UtilsService) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['data']?.currentValue) {
      // Update displayed data
      this.updateBoxList(changes['data'].currentValue.quoteData);
      // Update current trend
      this.currentTrend = this.utilsService.getTrendOf(changes['data'].currentValue.quoteData[QuoteKeysEnum.CHANGE]);
    }
  }

  updateBoxList(quoteData: QuoteDataModel): void {
    this.dataBoxToShow = [];
    for (let quote of this.overviewQuotes) {
      if (quoteData[quote.key]) {
        this.dataBoxToShow.push({
          name: quote.name,
          value: quoteData[quote.key],
          type: quote.type
        });
      };
    }
  }

  onRemoveStock(): void {
    this.removeCard.emit();
    this.stockListObsService.removeFromStockListBySymbol(this.data.symbol);
  }

  onClickRedirection(): void {
    this.router.navigate(['/sentiment', this.data.symbol]);
  }

}
