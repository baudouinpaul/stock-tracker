export interface StockCardBoxInterface {
    name: string;
    value: number;
    type: CardTypeEnum;
}

export enum CardTypeEnum {
    MONEY = "money",
    PCT = "pct"
}

export enum TrendEnum {
    UP = "up",
    DOWN = "down",
    STABLE = "stable"
}