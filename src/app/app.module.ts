// Modules
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
// Components
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SearchCardComponent } from './home/search-card/search-card.component';
import { StockCardComponent } from './home/stock-card/stock-card.component';
import { SentimentInformationsComponent } from './sentiment/sentiment-informations/sentiment-informations.component';
import { SentimentComponent } from './sentiment/sentiment.component';



@NgModule({
  declarations: [
    AppComponent,
    SearchCardComponent,
    StockCardComponent,
    HomeComponent,
    SentimentComponent,
    SentimentInformationsComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
  ],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
