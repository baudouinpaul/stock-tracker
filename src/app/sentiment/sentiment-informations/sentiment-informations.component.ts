import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { TrendEnum } from 'src/app/home/stock-card/stock-card.model';
import { SentimentDataModel } from 'src/shared/models/sentiment.model';
import { UtilsService } from 'src/shared/services/utils.service';

@Component({
  selector: 'app-sentiment-informations',
  templateUrl: './sentiment-informations.component.html',
  styleUrls: ['./sentiment-informations.component.scss']
})
export class SentimentInformationsComponent implements OnChanges {


  currentTrend: TrendEnum;
  monthName: string;
  TrendEnum = TrendEnum;

  @Input() monthData: SentimentDataModel;

  constructor(private utilsService: UtilsService) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['monthData'].currentValue) {
      // Update currentTrend
      this.currentTrend = this.utilsService.getTrendOf(changes['monthData'].currentValue.change);
      // Update montName
      this.monthName = this.utilsService.getMonthNameByNumber(this.monthData.month - 1);
    }
  }

}
