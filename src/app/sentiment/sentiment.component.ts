import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SentimentDataModel, SentimentModel } from 'src/shared/models/sentiment.model';
import { CompanyNameResultModel } from 'src/shared/models/stock.model';
import { StockService } from 'src/shared/services/stock.service';

@Component({
  selector: 'app-sentiment',
  templateUrl: './sentiment.component.html',
  styleUrls: ['./sentiment.component.scss']
})
export class SentimentComponent implements OnInit, OnDestroy {

  symbol: string;
  name: string;
  isLoading: boolean;
  threeMonthSentiments: SentimentDataModel[];
  subscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private stockService: StockService,
    private router: Router
  ) { }

  ngOnInit() {
    this.subscription = this.route.params.subscribe(params => {
      this.symbol = params['symbol'];
      this.threeMonthSentiments = [];
      this.name = null;
      this.updateLast3MonthSentimentData();
    });
  }

  updateLast3MonthSentimentData(): void {
    this.isLoading = true;
    // get dates
    const today: Date = new Date();
    let todayMinus2Month: Date = new Date();
    todayMinus2Month.setMonth(todayMinus2Month.getMonth() - 2);
    // Format to yyyy-mm-dd
    const fromStr: string = todayMinus2Month.toLocaleDateString('en-CA');
    const toStr: string = today.toLocaleDateString('en-CA');
    this.stockService.getSentimentData(this.symbol, fromStr, toStr).subscribe({
      next: (response) => {
        const companyName = (response.name as CompanyNameResultModel)?.result?.find((nameResult) => nameResult.displaySymbol === this.symbol)?.description;
        if (companyName && response?.sentimentData) {
          this.name = companyName;
          this.updateSentimentsArray(todayMinus2Month, response.sentimentData);
        } else {
          this.name = "Ooops :(";
          this.isLoading = false;
        }
      },
      error: (error) => {
        this.isLoading = false;
      }
    });
  }

  updateSentimentsArray(from: Date, sentimentData: SentimentModel): void {
    this.threeMonthSentiments = [];
    // Loop to find data for each month
    for (let i = 0; i < 3; i++) {
      const current: Date = new Date(from.getTime());;
      current.setMonth(from.getMonth() + i);
      const month = current.getMonth() + 1; // getMonth : january is 0
      const year = current.getFullYear();
      // Patch value
      let monthData: SentimentDataModel = sentimentData.data?.find((data) => data.year === year && data.month === month);
      if (!monthData) {
        // Minimum known values
        monthData = {
          year: year,
          month: month,
          change: null,
          mspr: null,
          symbol: this.symbol
        };
      }
      this.threeMonthSentiments.push(monthData);
    }
    this.isLoading = false;
  }

  onClickGoBack(): void {
    this.router.navigate(['']);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
