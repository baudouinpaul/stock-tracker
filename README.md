# StockTracker

Current version : 1.1.0

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Deploy on Gitlab Pages

Run a pipeline with variable `PAGES_DEPLOY` set to true. Gitlab pages website will be available at https://baudouinpaul.gitlab.io/stock-tracker/.
